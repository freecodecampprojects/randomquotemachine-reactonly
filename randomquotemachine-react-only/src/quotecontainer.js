import React from 'react';
import './quotecontainer.css';
import Quote from './quote.js'
import $ from 'jquery';


class QuoteContainer extends React.Component {
  constructor(props){
    super(props)
    this.state ={
      text : "",
      author: ""
    };
    this.generateNewQuote = this.generateNewQuote.bind(this);
    this.requestQuote = () => $.ajax({
      url: "https://api.forismatic.com/api/1.0/",
      jsonp: "jsonp",
      dataType: "jsonp",
      data: {
        method: "getQuote",
        lang: "en",
        format: "jsonp"
      }
    })
    .done(function(response){
      this.setState({text: response.quoteText, author : response.quoteAuthor})
    }.bind(this))
    .fail(function(){
    });
  }
  
  generateNewQuote(){
    var hue = Math.floor(Math.random() * 360);
    var pastel = 'hsl(' + hue + ', 100%, 40%)';
    $("#root").css("background-color", pastel.toString());
    $("button").css("background-color", pastel.toString());
    $(".quote").css("color", pastel );
    $(".quote").animate({opacity: "0"}).animate({opacity: "1"});

    this.requestQuote();
  };

  componentDidMount(){
    var hue = Math.floor(Math.random() * 360);
    var pastel = 'hsl(' + hue + ', 100%, 40%)';
    $("#root").css("background-color", pastel.toString());
    $("button").css("background-color", pastel.toString());
    $(".quote").css("color", pastel );
    this.requestQuote();
  }

  render() {
    const twitterIcon = <i className ="fab fa-twitter"></i>;
    const tumblrIcon = <i className = "fab fa-tumblr"></i>;
    const tweet = "http://twitter.com/intent/tweet?text=" + this.state.text + " -" + this.state.author;
    return (
      
      <div id="quote-box" className="quote-container">
        <Quote text={this.state.text} author={this.state.author}></Quote>
        <div className="button-group">
          <a id="tweet-quote" href={tweet} target="_blank"><button  className="btn bnt-default twitter-button">{twitterIcon}</button></a>
          <button id="new-quote" className="btn bnt-default new-button" onClick={this.generateNewQuote}>New Quote</button>
        </div>
      </div>
    )
  }
}

export default QuoteContainer;
