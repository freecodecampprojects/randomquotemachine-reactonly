import React from 'react';
import './quote.css';

class Quote extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        const text = <i className="fas fa-quote-left"> {this.props.text}</i>;

        return (
            <div className="quote">
                <p id="text" className="quote-text">{text}</p>
                <p  id="author" className="quote-author">{this.props.author}</p>
            </div>
        )
    };
}

export default Quote;