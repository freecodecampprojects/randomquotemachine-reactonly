import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import QuoteContainer from './quotecontainer';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<QuoteContainer />, document.getElementById('root'));
registerServiceWorker();
